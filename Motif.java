import java.util.Random;
import java.util.Scanner;


/**
 * @author Jacqueline Schmidt & Frauke Adam
 *
 */
public class Motif {

	public static String motif = "";
	public static String [] seq = null;
	
	public Motif(){
		this.motif = motif;
		this.seq = seq;
	}
	
	
	
	/**
	 * @return the motif
	 */
	public String getMotif() {
		return motif;
	}



	/**
	 * @param motif the motif to set
	 */
	public void setMotif(String motif) {
		this.motif = motif;
	}

	/**
	 * @return the seq
	 */
	public String[] getSeq() {
		return seq;
	}

	/**
	 * @param seq the seq to set
	 */
	public void setSeq(String[] seq) {
		this.seq = seq;
	}

	public static void main(String [] args){
		
	Motif M = new Motif();
	M.generateMotif(motif, 5, 1);
	M.generateSequence(5, 20);
	String m = M.getMotif();
	String[] s = M.getSeq();
	M.searchMotif(s, m);
	}
	
	/**
	 * Method to generate 'k' random sequences with 'n' characters.
	 * @param blub
	 * @param list
	 * @param k
	 * @param n
	 * @return
	 */
	public String[] generateSequence( int k, int n){
		char[] zeichen = {'A', 'T', 'C', 'G'};
		Random rand = new Random();
		char s;
		int a;
		String[] seq = new String[k];  
		for(int j=0; j<k; j++){
			seq[j] = "";
		}
		
		for ( a=k; a>0; a--){
		for (int i=0; i<n;i++)
		{
			int zahl = rand.nextInt(4);
		    s = zeichen[zahl];
		    seq[a-1]  +=  s;
		}
		//System.out.println(seq[a-1]);
		}
		return seq;
	}
	
	/**
	 * Either the user can provide a Motif or if he doesn't 
	 * the method generates a random motif of length l
	 * To generate a random motif the method generateSequence is called
	 * @param list
	 * @param blub
	 * @param l
	 * @param d
	 * @return
	 */
	public String generateMotif(String motif, int l, int d){
		motif = "";
		Scanner scan = new Scanner(System.in);
		System.out.println("Please type a Motif : (A random motif will be generated if there is no input!) ");
		String s = scan.nextLine();
		motif = s;
		scan.close();
		if(s.isEmpty()){
			char[] zeichen = {'A', 'T', 'C', 'G'};
			Random rand = new Random();
			char f;
			
			for (int i=0; i<l;i++)
			{
				int zahl = rand.nextInt(4);
			    f = zeichen[zahl];
			    motif = motif + f;
			}
			}
		return motif;
		}

	/**
	 * 
	 * Method which searches all sequences to find the motif in each of them 
	 * and returns the position of the motif in each sequence
	 * @param list
	 * @param blub
	 */
	
	public void searchMotif(String [] seq, String motif){
		String moo = this.getMotif(); 
		System.out.println(moo);
		/*for (int i=0; i<list.length();i++)
		   if(list.contains(blub)){
			   System.out.println(blub);
			System.out.println("hurra");
		    break;
		    }
		*/
	}
	
}
